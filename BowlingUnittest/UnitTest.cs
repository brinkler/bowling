using System;
using System.Linq;
using Bowling;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace BowlingUnitTest{
    [TestClass]
    public class BowlingGameTest{

        Game game;

        [TestInitialize]
        public void Setup() => game = new Game();

        [TestMethod]
        public void TestCreateGame(){
            Assert.IsNotNull(game);
        }

        [TestMethod]
        public void TestZeroGame(){
            game.Fill(() => (0, 0));
            Assert.IsNotNull(game);

            Assert.AreEqual(0, game.CalculateScore());
        }

        [TestMethod]
        public void Test7Game(){
            game.Fill(() => (3, 4));

            Assert.AreEqual(70, game.CalculateScore());
        }

        [TestMethod]
        public void TestLastSpareFrame(){
            var game = new Game();
            IFrame lastFrame = game.AddLastFrame((5, 5));
            Assert.IsInstanceOfType(lastFrame, typeof(LastFrameSpare));
            ((ILastFrame) lastFrame).ExtraRoll = new int[1] {5};
            Assert.AreEqual(15, game.CalculateScore());
        }

        [TestMethod]
        public void TestLastStrikeFrame(){
            IFrame lastFrame = game.AddLastFrame((10, 0));
            Assert.IsInstanceOfType(lastFrame, typeof(LastFrameStrike));
            ((ILastFrame) lastFrame).ExtraRoll = new int[2] {0, 5};
            Assert.AreEqual(15, game.CalculateScore());
        }

        [TestMethod]
        public void TestLastStrike17Frame(){
            IFrame lastFrame = game.AddLastFrame((10, 0));
            Assert.IsInstanceOfType(lastFrame, typeof(LastFrameStrike));
            ((ILastFrame) lastFrame).ExtraRoll = new int[2] {3, 4};
            Assert.AreEqual(17, game.CalculateScore());
        }

        [TestMethod]
        public void TestLastSuperStrikeFrame(){
            IFrame lastFrame = game.AddLastFrame((10, 0));
            Assert.IsInstanceOfType(lastFrame, typeof(LastFrameStrike));
            ((ILastFrame) lastFrame).ExtraRoll = new int[2] {10, 10};
            Assert.AreEqual(30, game.CalculateScore());
        }

        [TestMethod]
        public void TestChuckNorrisGame(){
            game.Fill(() => (100, 100), lf => (100, 0));
            Assert.AreEqual(3900, game.CalculateScore());
        }

        [TestMethod]
        public void TestSpareGame(){
            var game = new Game();

            game.Fill(() => (5, 5), lastFrame => (5, 0));

            Assert.AreEqual(15, game.Frames[0].Sum());

            Assert.AreEqual(150, game.CalculateScore());
        }

        [TestMethod]
        public void TestStrikeAfterSpare(){
            var game = new Game();
            var spareFrame = game.AddFrame((6, 4));
            var strikeFrame = game.AddFrame((10, 0));
            if (spareFrame is Spare ) {
                ((Spare)spareFrame).FutureFrame(strikeFrame);
            }

            var sum = game.Frames[0].Sum();
            Assert.AreEqual(20, sum);
        }

        [TestMethod]
        public void TestStrikeGame(){

            game.Fill(() => (10, 0), lastFrame => (10, 10));
            var sum = (from frame in game.Frames select frame.Sum()).ToArray();
            var result = Enumerable.Repeat(30, 10).ToArray();

            for (var i = 0; i < sum.Count(); i++) {
                Assert.AreEqual(result[i], sum[i], "result[{0}] != sum[{0}]", i);
            }

            Assert.AreEqual(30, game.Frames[0].Sum());

            Assert.AreEqual(300, game.CalculateScore());
        }

        [TestMethod]
        public void TestComplicatedGame(){
            var i = 0;
            var rolls = new (int, int)[] {
                    (10, 0),
                    (10, 0),
                    (0, 10),
                    (10, 0),
                    (9, 1),
                    (5, 4),
                    (0, 0),
                    (6, 2),
                    (5, 4),
                    (10, 0)
            };
            game.Fill(() => rolls[i++], frame => (9, 1));
            Assert.AreEqual(141, game.CalculateScore());

        }

        [TestMethod]
        public void TestAssignmentGame(){
            var i = 0;
            var rolls = new (int, int)[] {
                    (1, 4),
                    (4, 5),
                    (6, 4),
                    (5, 5),
                    (10, 0),
                    (0, 1),
                    (7, 3),
                    (6, 4),
                    (10, 0),
                    (2, 8)
            };
            game.Fill(() => rolls[i++], frame => (6, 0));
            Assert.AreEqual(133, game.CalculateScore());
        }
    }
}