# README

This is my implementation of "The Bowling game"

# How to setup

Import into Visual Studio Code or Jetbrains Rider

The implemented solution consists of three projects:

- The Bowling project
- The Bowling Unittest project
- The ReSTApi I have added.

## The Bowling Game

Contains the implementation of the bowling game, which basically provides the ability to add a number of frames and calculate a score

## The Bowling Unittest

Contains a number of tests which I have used to
validate my results.

I have validated against games from:
[https://www.bowlinggenius.com/][bowling genius]

## The ReST Api

I have created a simple ReST Api.

- The webui is pretty rudementary it will accept one get method `http://localhost:44339/api/Values` and will return a bit of json:

  ```
      [{"rolls":[8,2,10],"score":20,"totalScoreSoFar":20},{"rolls":[10,10,7],"score":27,"totalScoreSoFar":47},{"rolls":[10,7,1],"score":18,"totalScoreSoFar":65},{"rolls":[7,1],"score":8,"totalScoreSoFar":73},{"rolls":[0,4],"score":4,"totalScoreSoFar":77},{"rolls":[8,0],"score":8,"totalScoreSoFar":85},{"rolls":[7,2],"score":9,"totalScoreSoFar":94},{"rolls":[9,1,7],"score":17,"totalScoreSoFar":111},{"rolls":[7,0],"score":7,"totalScoreSoFar":118},{"rolls":[7,0],"score":7,"totalScoreSoFar":125}]
  ```

  Each element

  `{"rolls":[10,10,7],"score":27,"totalScoreSoFar":47}`

  is a representation of the "rolls" comprising the score calulation. In this case a strike followed by a strike and a 7 in the first roll of the third frame. The Score was 27 for the frame and total so far is 47.

## Design

Looking at the mechanics of the bowling game a few concepts were identified:

- A class named Game would probably be a good concept to have
  - A Game consists of 10 Frames - so the concept of a frame should probably be modelled.
  - Game provides the ability to `Fill` the game and the ability to `CalculateScore` for the game.
- A few frame could be a few different things so specializations would be in order.
  - Normal frame - `Frame`
  - Spare frame - `Spare`
  - Strike frame `Strike`
- The last frame can be a little different
  - Can be a normal frame
  - Can be a Spare in which case the player needs to have one more attempt
  - Can be a Strike and then the user needs to have two more attempts.

These rules have been captured in the classes. A `Spare` or a `Strike` implments a future value captured in the inteface `IFutureDependent`. Similar for the last frame which has been captured by `ILastFrame`. The frame `IFrame` interface describes three concepts:

- Sum - the sum of the fame
- (first, second) - the rolls done in the current frame
- ComprisingRolls - The rolls which comprise the score
  - Normal frame will have comprising rolls of (first, second)
  - Spare frame will have (first, second) and first roll in next
  - Strike will have the (first and the next two rolls so either (first, second) or (first) and then first of the subsequent frame if the next frame is a strike.
    The comprising rolls is what is exposed in the webapi project.

All of the different frames have been modelled as classes in the `Frame.cs` file. `Game` is found in `Game.cs`

## Implementation

Not being too familiar with C# I have seized the opportunity to experiment with some of the features C# offer

- Extension methods
- Null propagation
- Inline variable declaration
- Built-in primitive support for tuples `(one,two)`
- Lambda/Delegation
- Linq
- Unittests
- Experimented with Visual Studio 2019
- Installed C# tools from JetBrains
- Visual Studio Code
- Atlassian BitBucket C# pipeline which compiles the project and runs the unittests.
- Mac version of Visual Studio

## Future steps to look into

- GraphQL seems like it is also very well supported so that could be interesting. Ran into a bit of a problem with my version of `Visual Code Studio`
- Docker container - Also gave this a run but my home windows does not support docker since it is not a professional edition and I did not want to experiment with this on my Mac laptop.

[bowling genius]: https://www.bowlinggenius.com/
