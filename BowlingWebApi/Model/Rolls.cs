﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Bowling;

namespace BowlingWebApi.Model {

    public class FrameDO {
        public List<int> Rolls { get; }
        public int Score { get; }
        public int TotalScoreSoFar { get; }

        public FrameDO(IFrame frame, int totalScoreSoFar) {
            Rolls = frame.ComprisingRolls();
            Score = frame.Sum();
            TotalScoreSoFar = totalScoreSoFar;
        }
    }
}
