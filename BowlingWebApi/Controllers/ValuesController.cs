﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Bowling;
using BowlingWebApi.Model;
using Microsoft.AspNetCore.Authorization.Infrastructure;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace BowlingWebApi.Controllers {
    [Route("api/[controller]")]
    [ApiController]
    public class ValuesController : ControllerBase {
        

        // GET: api/<ValuesController>
        [HttpGet]
        public IEnumerable<FrameDO> GetRandom() {
            // Generate random game
            Random random = new Random();
            Game game = new Game();
            game.Fill(() => {
                CreateRoll(random, out int firstroll, out int secondroll);
                return (firstroll, secondroll);
            }, lastFrame => {
                if (lastFrame is Strike) {
                    CreateRoll(random, out int firstroll, out int secondroll);
                    return (firstroll, secondroll);
                } else if (lastFrame is Spare) {
                    return (FirstRoll(random), 0);
                }
                return (0, 0);
            });

            List<FrameDO> result = new List<FrameDO>(10);
            int totalScore = 0;
            foreach (var frame in game.Frames) {
                totalScore += frame.Sum();
                result.Add(item: new FrameDO(frame, totalScore));
            }

            return result;
        }

        private static void CreateRoll(Random random, out int firstroll, out int secondroll) {
            firstroll = FirstRoll(random);
            secondroll = random.Next(0, 11-firstroll);
        }

        private static int FirstRoll(Random random) {
            return random.Next(11);
        }
    }
}
