﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Bowling{
    public class Game{
        public Game(){
            Frames = new List<IFrame>();
        }

        public IList<IFrame> Frames { get; }

        public int CalculateScore(){
            // First naive implementation
            var result = Frames.Sum(f => f.Sum());
            return result;
        }

        public Frame AddFrame((int, int) tuple){
            var frame = AddFrame(tuple,
                                 t => new Strike(t),
                                 t => new Spare(t),
                                 t => new Frame(t));
            return frame;
        }

        public ILastFrame AddLastFrame((int, int) tuple){
            var frame = AddFrame<ILastFrame>(tuple,
                                             t => new LastFrameStrike(t),
                                             t => new LastFrameSpare(t),
                                             t => new LastFrame(t)
                                            );
            return frame;
        }

        private T AddFrame<T>((int, int) tuple,
                              Func<(int, int), T> createStrike,
                              Func<(int, int), T> createSpare,
                              Func<(int, int), T> createFrame)
                where T : IFrame{
            T frame;
            if (tuple.Item1 >= 10) // Strike - >= to make sure Chuck Norris does not destroy the Bowling Alley
                frame = createStrike(tuple);
            else if (tuple.Item1 + tuple.Item2 == 10) // Spare
                frame = createSpare(tuple);
            else
                frame = createFrame(tuple);

            Frames.Add(frame);
            return frame;
        }

        public ILastFrame Fill(Func<(int, int)> p){
            IFutureDependent spareOrStrike = null;
            for (var i = 0; i < 9; i++) {
                var added = AddFrame(p());
                if (spareOrStrike != null) {
                    spareOrStrike.FutureFrame(added);
                    spareOrStrike = null;
                }

                if (added is IFutureDependent frame) spareOrStrike = frame;
            }

            // For last frame we do not have a future frame
            var lastFrame = AddLastFrame(p());
            spareOrStrike?.FutureFrame(lastFrame);

            return lastFrame;
        }

        public ILastFrame Fill(Func<(int, int)> p, Func<ILastFrame, (int, int)> lastP){
            var lastFrame = Fill(p);

            // For last frame we do not have a future frame
            if (lastFrame is LastFrameStrike || lastFrame is LastFrameSpare)
                lastFrame.ExtraRoll = lastP(lastFrame).ToArray();
            return lastFrame;
        }
    }
}