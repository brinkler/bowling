﻿using System;
using System.Linq;

namespace Bowling{
    internal static class Program{
        private static void Main(string[] args){
            Console.WriteLine("Please run the unit test.");

            Console.WriteLine("Example score from assignment");
            var game = new Game();
            var i = 0;
            var rolls = new (int, int)[] {
                    (1, 4),
                    (4, 5),
                    (6, 4),
                    (5, 5),
                    (10, 0),
                    (0, 1),
                    (7, 3),
                    (6, 4),
                    (10, 0),
                    (2, 8)
            };
            game.Fill(() => rolls[i++], frame => (6, 0));
            
            var sum = (from frame in game.Frames select frame.Sum()).ToArray();

            var score = 0;
            for (var j= 0; j < sum.Count(); j++) {
                score += sum[j];
                Console.WriteLine("{0} : Score [{1}]. Total : {2}", j, sum[j].ToString(), score);
            }
            Console.WriteLine("Result of game should be 133 : {0}", game.CalculateScore().ToString());
        }
    }
}