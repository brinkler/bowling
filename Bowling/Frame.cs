﻿using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace Bowling {
    /// <summary>
    /// An extension function used to add the ToArray method on (int,int) tuples.
    /// </summary>
    public static class TupleExtensions{
        public static int[] ToArray(this (int first, int second) t){
            var (first, second) = t;
            return new[] {first, second};
        }
    }

    /// <summary>
    ///     A frame of a bowling game.
    /// </summary>
    public interface IFrame{
        /// <summary>
        /// Property holding the values of the first and second roll.
        /// </summary>
        (int first, int second) Tuple { get; }
        int Sum();
        /// <summary>
        /// Get the values this frame is using for score calculation
        /// </summary>
        List<int> ComprisingRolls();
    }

    /// <summary>
    /// The last frame of a game is a little more interesting than the first ones.
    /// </summary>
    public interface ILastFrame : IFrame{
        /// <summary>
        /// Add extra rolls to the frame
        /// </summary>
        int[] ExtraRoll { set; }
    }

    /// <summary>
    ///     Marker interface indicating that the frame is dependant on the result of a future frame
    /// </summary>
    public interface IFutureDependent : IFrame {
        IFrame FutureFrame(IFrame futureFrame);
    }

    /// <summary>
    ///     A Normal frame does not depend on next frame in any way. The score of a normal frame is at most 9
    /// </summary>
    public class Frame : IFrame{
        public Frame((int first, int second) tuple){
            Tuple = tuple;
        }

        public (int first, int second) Tuple { get; }

        public int Sum(){
            return ComprisingRolls().Sum();
        }

        public virtual List<int> ComprisingRolls(){
            return new List<int>(Tuple.ToArray());
        }
    }

    public class Spare : Frame, IFutureDependent{
        public Spare((int first, int second) tuple) : base(tuple){ }

        protected IFrame NextFrame { get; set; }

        public IFrame FutureFrame(IFrame futureFrame){
            NextFrame = futureFrame;
            return this;
        }

        public override List<int> ComprisingRolls(){
            var list = new List<int>(base.ComprisingRolls());
            if (NextFrame == null) return list;
            var futureRolls = NextFrame.ComprisingRolls();
            list.Add(futureRolls[0]);
            return list;
        }
    }

    public class Strike : Spare {
        public Strike((int first, int second) tuple) : base(tuple){ }

        public override List<int> ComprisingRolls(){
            var enumerable = from roll in Tuple.ToArray() where roll != 0 select roll;
            
            var list = new List<int>(enumerable);
            if (NextFrame == null) return list;
            var futureRolls = NextFrame.ComprisingRolls();
            list.Add(futureRolls[0]);
            list.Add(futureRolls[1]);

            return list;
        }
    }

    public class LastFrame : Frame, ILastFrame{
        public LastFrame((int first, int second) tuple) : base(tuple){ }

        public int[] ExtraRoll { get; set; }
    }

    public class LastFrameStrike : Strike, ILastFrame{
        public LastFrameStrike((int first, int second) tuple) : base(tuple){ }

        public int[] ExtraRoll { get; set; }

        public override List<int> ComprisingRolls(){
            var result = base.ComprisingRolls();
            result.AddRange(ExtraRoll);
            return result;
        }
    }

    public class LastFrameSpare : Spare, ILastFrame{
        public LastFrameSpare((int first, int second) tuple) : base(tuple){ }

        public int[] ExtraRoll { get; set; }

        public override List<int> ComprisingRolls(){
            var result = base.ComprisingRolls();
            result.Add(ExtraRoll[0]);
            return result;
        }
    }
}